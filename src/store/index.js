import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    cartList: []//存储所有加入购物车的数据
  },
  mutations: {//用来修改state,同步操作(action)
    addToCart(state, payload) {//加入购物车

      //商品对象如果初次加入购物车，数量为1，否则数量加1
      //判断商品是否重复加入
      var findItem = state.cartList.find((item, index) => {
        return item.id === payload.id
      })
      if (findItem) {//重复加入
        findItem.count++;
      } else {//初次加入
        //不能用直接赋值的方式，添加属性 payload.count = 1
        Vue.set(payload, "count", 1);
        state.cartList.push(payload)
      }
    },
    add_count(state, id) {//数量加1
      var findItem = state.cartList.find((item, index) => { return item.id === id })
      if (findItem) {//找到了需要加1 的商品
        findItem.count++
      }
    },
    sub_count(state, id) {//数量加1
      var findItem = state.cartList.find((item, index) => { return item.id === id })
      if (findItem.count > 1 && findItem) {//找到了需要加1 的商品
        findItem.count--
      } else if (findItem.count === 1) {//从购物车移除商品
        //查找需要删除的商品在 购物车列表中的索引
        var findIndex = state.cartList.findIndex((item, index) => { return item.id === id })
        //从购物车列表删除
        state.cartList.splice(findIndex, 1)
      }
    }
  },
  actions: {
  },
  modules: {
  }
})
