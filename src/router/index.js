import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from "../views/Cart/Home/Home"
import Cart from '../views/Cart/Cart/Cart'
import Confirm from '../views/Cart/Confirm/Confirm'
import Detail from '../views/Cart/Detail/Detail'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home


  },
  {
    path: "/cart",
    name: "cart",
    component: Cart
  },
  {
    path: "/confirm",
    name: "confirm",
    component: Confirm
  },{
    path:"/detail",
    name:"detail",
    component:Detail
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
