import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

Vue.config.productionTip = false

//引入Header组件，注册
import Header from "./components/Header"


Vue.component("Header", Header)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
